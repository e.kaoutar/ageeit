package com.example.ageeit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Listeetudiants extends AppCompatActivity implements AdapterView.OnItemSelectedListener{


    public ListView listetuds;
    public String nomactiv;

    //array of activities
    ArrayList<String> students = new ArrayList<>();

    class dbWorkerEtuds extends AsyncTask {
        private Context c;
        private AlertDialog ad;


        //context
        public dbWorkerEtuds(Context c){
            this.c = c;
        }


        @Override
        protected Object doInBackground(Object[] objects) {
            String cible = "http:192.168.0.118/phpscriptlab1/listeetuds.php";
            try{
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("nomactiv", "utf-8")+"="+
                        URLEncoder.encode((String)objects[0], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){
                    sbuff.append(line + "\n");
                }

                String[] separated = sbuff.toString().split(";");
                //fill arraylist
                for(int i=0; i<separated.length; i++){
                    students.add(separated[i]);
                }

                //Log.d("etudiants", sbuff.toString());

                return sbuff.toString();

            }
            catch (Exception ex){
                return ex.getMessage();
            }
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listeetudiants);

        //define controls
        listetuds = findViewById(R.id.listetuds);

        //fill spinner
        Spinner sp = (Spinner)findViewById(R.id.spinactivity);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.activities, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapter);
        sp.setOnItemSelectedListener(this);

    }



    public void btnAfficherEtuds(View view){
        dbWorkerEtuds dbe = new dbWorkerEtuds(this);
        dbe.execute(nomactiv);

        //calling adapter:
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, students);
        listetuds.setAdapter(arrayAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        nomactiv = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
