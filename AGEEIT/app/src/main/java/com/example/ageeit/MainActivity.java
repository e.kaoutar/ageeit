package com.example.ageeit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {

    private EditText txtUser;
    private EditText txtPass;
    private EditText txtNom;
    private EditText txtPrenom;
    private EditText txtEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUser = (EditText)findViewById(R.id.edtUsername);
        txtPass = (EditText)findViewById(R.id.edtPassword);
        txtNom = (EditText)findViewById(R.id.edtName);
        txtPrenom = (EditText)findViewById(R.id.edtFirstname);
        txtEmail = (EditText)findViewById(R.id.edtEmail);

    }

    public void btnEnroll(View view){

        String userN = this.txtUser.getText().toString();
        String userP = this.txtPass.getText().toString();
        String userNm = this.txtNom.getText().toString();
        String userPm = this.txtPrenom.getText().toString();
        String userE = this.txtEmail.getText().toString();

        class dbWorker extends AsyncTask{
            private Context c;
            private AlertDialog ad;

            @Override
            protected void onPreExecute(){
                this.ad = new AlertDialog.Builder(this.c).create();
                this.ad.setTitle("Inscription status");
            }

            //context
            public dbWorker(Context c){
                this.c = c;
            }


            @Override
            protected Object doInBackground(Object[] objects) {
                String cible = "http:192.168.0.118/phpscriptlab1/inscription.php";
                try{
                    URL url = new URL(cible);
                    HttpURLConnection con = (HttpURLConnection)url.openConnection();
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestMethod("POST");

                    OutputStream outs = con.getOutputStream();
                    BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                    String msg = URLEncoder.encode("user", "utf-8")+"="+
                            URLEncoder.encode((String)objects[0], "utf-8")+"&"+
                            URLEncoder.encode("pw", "utf-8")+"="+
                            URLEncoder.encode((String)objects[1], "utf-8")+"&"+
                            URLEncoder.encode("nom", "utf-8")+"="+
                            URLEncoder.encode((String)objects[2], "utf-8")+"&"+
                            URLEncoder.encode("prenom", "utf-8")+"="+
                            URLEncoder.encode((String)objects[3], "utf-8")+"&"+
                            URLEncoder.encode("email", "utf-8")+"="+
                            URLEncoder.encode((String)objects[4], "utf-8");

                    bufw.write(msg);
                    bufw.flush();
                    bufw.close();
                    outs.close();

                    InputStream ins = con.getInputStream();
                    BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                    String line;
                    StringBuffer sbuff = new StringBuffer();

                    while((line = bufr.readLine()) != null){
                        sbuff.append(line + "\n");
                    }
                    return sbuff.toString();

                }
                catch (Exception ex){
                    return ex.getMessage();
                }
            }

            @Override
            protected void onPostExecute(Object o){

                this.ad.setMessage("Inscription OK");
                this.ad.show();
            }


        }


        dbWorker dbw = new dbWorker(this);
        dbw.execute(userN, userP, userNm, userPm, userE);



    }


    public void btnRedirectConnexion(View view){
        Intent i = new Intent(this, Login.class);
        startActivity(i);
    }

}
