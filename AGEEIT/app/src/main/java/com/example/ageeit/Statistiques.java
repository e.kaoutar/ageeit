package com.example.ageeit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class Statistiques extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public String nomactivite;
    public TextView titlestats;
    public TextView resultstats;
    public TextView ratestats;
    public String moyenne;
    public String nbrInscripActiv;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistiques);

        //define stats
        titlestats = findViewById(R.id.txtresultsstats1);
        resultstats = findViewById(R.id.txtresultsstats2);
        ratestats = findViewById(R.id.txtresultsstats3);

        //fill spinner
        Spinner sp = (Spinner)findViewById(R.id.spinactivity);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.activities, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapter);
        sp.setOnItemSelectedListener(this);
    }

    //send activity name to database
    class dbWorkerStats extends AsyncTask {
        private Context c;
        private AlertDialog ad;


        //context
        public dbWorkerStats(Context c){
            this.c = c;
        }


        @Override
        protected Object doInBackground(Object[] objects) {
            String cible = "http:192.168.0.118/phpscriptlab1/statsactiv.php";
            try{
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("nomactiv", "utf-8")+"="+
                        URLEncoder.encode((String)objects[0], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){
                    sbuff.append(line + "\n");
                }

                nbrInscripActiv = sbuff.toString();

                Log.d("stats", sbuff.toString());


                return sbuff.toString();

            }
            catch (Exception ex){
                return ex.getMessage();
            }
        }

    }

    public void btnAfficherStats(View view){
        dbWorkerStats dbw = new dbWorkerStats(this);
        dbw.execute(nomactivite);
        titlestats.setTextColor(Color.parseColor("#40A497"));
        titlestats.setSelected(true);
        titlestats.setText("Les statistiques de cette activité:");

        resultstats.setText("Le nombre d'inscriptions de cette activité est de "+ nbrInscripActiv);

    }

    class dbWorkerAverage extends AsyncTask {
        private Context c;
        private AlertDialog ad;


        //context
        public dbWorkerAverage(Context c){
            this.c = c;
        }


        @Override
        protected Object doInBackground(Object[] objects) {
            String cible = "http:192.168.0.118/phpscriptlab1/averageactivity.php";
            try{
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("nomactiv", "utf-8")+"="+
                        URLEncoder.encode((String)objects[0], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){
                    sbuff.append(line + "\n");
                }

                moyenne = sbuff.toString();

                Log.d("avg", sbuff.toString());

                return sbuff.toString();

            }
            catch (Exception ex){
                return ex.getMessage();
            }
        }

    }

    public void btnAfficherMoyenne(View view){

        dbWorkerAverage dva = new dbWorkerAverage(this);
        dva.execute(nomactivite);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("La moyenne de cette activité est de : "+ moyenne);
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        String lactivite = parent.getItemAtPosition(position).toString();
        nomactivite=lactivite;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        //Si aucune activite n'est selectionne on aura la premiere selectionnee par defaut pour eviter que le pointeur soit null.
        nomactivite = adapterView.getItemAtPosition(0).toString();
    }
}
