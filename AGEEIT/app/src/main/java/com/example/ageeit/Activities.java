package com.example.ageeit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Activities extends AppCompatActivity {

    public TextView username;
    public TextView password;
    public ListView listactivities;
    public EditText nomactiv;
    public EditText descactiv;

    //load preferences to get username and password to fetch activities
    public void loadPrefs(TextView user, TextView pass){
        int mode = Activity.MODE_PRIVATE;
        SharedPreferences myPrefs = getSharedPreferences("userInfos", mode);
        String user_value = myPrefs.getString("userS", "");
        String pass_value = myPrefs.getString("pwS", "");

        user.setText(user_value);
        pass.setText(pass_value);
    }

    //array of activities
    ArrayList<String> activities = new ArrayList<>();

    //async
    class dbWorker extends AsyncTask {
        private Context c;
        private AlertDialog ad;


        //context
        public dbWorker(Context c){
            this.c = c;
        }


        @Override
        protected Object doInBackground(Object[] objects) {
            String cible = "http:192.168.0.118/phpscriptlab1/myactivities.php";
            try{
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("user", "utf-8")+"="+
                        URLEncoder.encode((String)objects[0], "utf-8")+"&"+
                        URLEncoder.encode("pw", "utf-8")+"="+
                        URLEncoder.encode((String)objects[1], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){

                        sbuff.append(line + "\n");
                }


                String[] separated = sbuff.toString().split(";");


                //fill arraylist
                for(int i=0; i<separated.length; i++){
                    activities.add(separated[i] );
                }

                //Log.d("activities", separated[0]);

                return sbuff.toString();

            }
            catch (Exception ex){
                return ex.getMessage();
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activities);

        username= (TextView)findViewById(R.id.txtusername);
        password = (TextView)findViewById(R.id.txtpassword);
        listactivities = (ListView)findViewById(R.id.listactivities);
        nomactiv = (EditText)findViewById(R.id.edtnomactiv);
        descactiv = (EditText)findViewById(R.id.edtdescactiv);

        //loading preferences
        loadPrefs(username, password);
        dbWorker dbw = new dbWorker(this);
        dbw.execute(username.getText().toString(), password.getText().toString());


        //calling adapter:
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, activities);
        listactivities.setAdapter(arrayAdapter);

    }


    //async copy paste -> activity notes
    class dbWorkerprop extends AsyncTask {
        private Context c;
        private AlertDialog ad;


        //context
        public dbWorkerprop(Context c){
            this.c = c;
        }

        @Override
        protected void onPreExecute() {
            this.ad = new AlertDialog.Builder(this.c).create();
            this.ad.setTitle("Merci!");
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            String cible = "http:192.168.0.118/phpscriptlab1/proposeractiv.php";
            try{
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("nomactiv", "utf-8")+"="+
                        URLEncoder.encode((String)objects[0], "utf-8")+"&"+
                        URLEncoder.encode("descactiv", "utf-8")+"="+
                        URLEncoder.encode((String)objects[1], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){
                    sbuff.append(line + "\n");
                }

                return sbuff.toString();
            }
            catch (Exception ex){
                return ex.getMessage();
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            this.ad.setMessage("Proposition soumise.");
            this.ad.setButton(Dialog.BUTTON_POSITIVE,"OK",new DialogInterface.OnClickListener(){

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }

            });
            this.ad.show();
        }
    }

    public void btnProposition(View view){

        dbWorkerprop dbp = new dbWorkerprop(this);
        dbp.execute(nomactiv.getText().toString(), descactiv.getText().toString());

    }



}
