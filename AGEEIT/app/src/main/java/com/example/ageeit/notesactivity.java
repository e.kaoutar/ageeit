package com.example.ageeit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class notesactivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    public TextView username;
    public TextView password;
    public EditText note;
    public EditText description;
    public String nomactivite;
    public String userss;
    public String passs;
    public String notee;
    public String descc;

    //load preferences to get username and password to fetch activities
    public void loadPrefs(TextView user, TextView pass){
        int mode = Activity.MODE_PRIVATE;
        SharedPreferences myPrefs = getSharedPreferences("userInfos", mode);
        String user_value = myPrefs.getString("userS", "");
        String pass_value = myPrefs.getString("pwS", "");

        user.setText(user_value);
        pass.setText(pass_value);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notesactivity);


        username = (TextView)findViewById(R.id.txtusernote);
        password = (TextView)findViewById(R.id.txtpassnote);
        note = (EditText)findViewById(R.id.edtnote);
        description = (EditText)findViewById(R.id.edtdesc);

        //loading preferences
        loadPrefs(username, password);

        //definir les string a envoye
        userss = username.getText().toString();
        passs = password.getText().toString();


        //fill spinner
        Spinner sp = (Spinner)findViewById(R.id.spinactivity);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.activities, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapter);
        sp.setOnItemSelectedListener(this);
    }

    //async notes
    class dbWorkernoter extends AsyncTask {
        private Context c;
        private AlertDialog ad;


        //context
        public dbWorkernoter(Context c){
            this.c = c;
        }

        @Override
        protected void onPreExecute() {
            this.ad = new AlertDialog.Builder(this.c).create();
            this.ad.setTitle("Merci!");
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            String cible = "http:192.168.0.118/phpscriptlab1/noteractiv.php";
            try{
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("user", "utf-8")+"="+
                        URLEncoder.encode((String)objects[0], "utf-8")+"&"+
                        URLEncoder.encode("pw", "utf-8")+"="+
                        URLEncoder.encode((String)objects[1], "utf-8")+"&"+
                        URLEncoder.encode("activnom", "utf-8")+"="+
                        URLEncoder.encode((String)objects[2], "utf-8")+"&"+
                        URLEncoder.encode("noteactiv", "utf-8")+"="+
                        URLEncoder.encode((String)objects[3], "utf-8")+"&"+
                        URLEncoder.encode("notedesc", "utf-8")+"="+
                        URLEncoder.encode((String)objects[4], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){
                    sbuff.append(line + "\n");
                }

                return sbuff.toString();
            }
            catch (Exception ex){
                return ex.getMessage();
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            this.ad.setMessage("Note soumise.");
            this.ad.setButton(Dialog.BUTTON_POSITIVE,"OK",new DialogInterface.OnClickListener(){

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }

            });
            this.ad.show();
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        String lactivite = parent.getItemAtPosition(position).toString();
        nomactivite=lactivite;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void btnNoter(View view){

        //Toast.makeText(this, username.getText().toString(), Toast.LENGTH_SHORT).show();
        notee = note.getText().toString();
        descc = description.getText().toString();
        dbWorkernoter dbn = new dbWorkernoter(this);
        dbn.execute(userss, passs, nomactivite, notee, descc);
        Toast.makeText(this, "Votre note est soumise.", Toast.LENGTH_SHORT).show();
    }

    public void btnCancelNote(View view){
        Intent i = new Intent(this, Dashboard.class);
        startActivity(i);
    }
}
