package com.example.ageeit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Login extends AppCompatActivity {

    public String loginState;
    public String bufContent;

    private EditText txtUser;
    private EditText txtPass;

    public void saveSharedPreferences(String txtUser, String txtPass){
        SharedPreferences myPrefs = getSharedPreferences("userInfos", MODE_PRIVATE);
        SharedPreferences.Editor editor = myPrefs.edit();

        editor.putString("userS", txtUser);
        editor.putString("pwS", txtPass);

        editor.apply();
        Toast.makeText(this, "Data saved", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUser = (EditText)findViewById(R.id.username);
        txtPass = (EditText)findViewById(R.id.password);


    }

    //asynctask

    class dbWorker extends AsyncTask {

        private Context c;
        private AlertDialog ad;

        public dbWorker(Context c){
            this.c = c;
        }

        @Override
        protected void onPreExecute(){
            this.ad = new AlertDialog.Builder(this.c).create();
            this.ad.setTitle("Login status");
        }


        @Override
        protected Object doInBackground(Object[] param) {
            String cible = "http:192.168.0.118/phpscriptlab1/connexion.php";
            try{
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("Username", "utf-8")+"="+
                        URLEncoder.encode((String)param[0], "utf-8")+"&"+
                        URLEncoder.encode("Password", "utf-8")+"="+
                        URLEncoder.encode((String)param[1], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){
                    sbuff.append(line + "\n");
                }

                //Log.d("result", sbuff.toString());

                bufContent = sbuff.toString();

                return sbuff.toString();

            }
            catch (Exception ex){
                return ex.getMessage();
            }
        }



        @Override
        protected void onPostExecute(Object o){

            this.ad.setMessage(bufContent);
            this.ad.show();
            //String dataLog = txtUser.toString()+" "+txtPass.toString();
            //if(o.equals(dataLog)){
              //  loginState="OK";
            //}
           // else{
               // loginState="ND";
            //}

        }


    }

    public void login(View view){

        dbWorker dbw = new dbWorker(this);
        dbw.execute(txtUser.getText().toString(), txtPass.getText().toString());


            Intent i = new Intent(this, Dashboard.class);
            startActivity(i);
            saveSharedPreferences(txtUser.getText().toString(), txtPass.getText().toString());


    }

    public void btninscrip(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }


}
