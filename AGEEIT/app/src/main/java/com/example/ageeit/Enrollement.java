package com.example.ageeit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Enrollement extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public TextView username;
    public TextView password;
    public String nomactivite;
    public String userrr;
    public String passss;

    public void loadPrefs(TextView user, TextView pass){
        int mode = Activity.MODE_PRIVATE;
        SharedPreferences myPrefs = getSharedPreferences("userInfos", mode);
        String user_value = myPrefs.getString("userS", "");
        String pass_value = myPrefs.getString("pwS", "");

        user.setText(user_value);
        pass.setText(pass_value);


    }

    class dbWorker extends AsyncTask {
        private Context c;
        private AlertDialog ad;

        @Override
        protected void onPreExecute(){
            this.ad = new AlertDialog.Builder(this.c).create();
            this.ad.setTitle("Inscription status");
        }

        //context
        public dbWorker(Context c){
            this.c = c;
        }


        @Override
        protected Object doInBackground(Object[] objects) {
            String cible = "http:192.168.0.118/phpscriptlab1/inscripactivite.php";
            try{
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("user", "utf-8")+"="+
                        URLEncoder.encode((String)objects[0], "utf-8")+"&"+
                        URLEncoder.encode("pw", "utf-8")+"="+
                        URLEncoder.encode((String)objects[1], "utf-8")+"&"+
                        URLEncoder.encode("nomactiv", "utf-8")+"="+
                        URLEncoder.encode((String)objects[2], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){
                    sbuff.append(line + "\n");
                }

                Log.d("erollement", sbuff.toString());

                return sbuff.toString();

            }
            catch (Exception ex){
                return ex.getMessage();
            }
        }

        @Override
        protected void onPostExecute(Object o){

            this.ad.setMessage("Vous êtes maintenant inscrit à l'activité "+ nomactivite);
            this.ad.show();
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrollement);

        username = findViewById(R.id.txtuserenroll);
        password = findViewById(R.id.txtpassenroll);

        loadPrefs(username, password);

        userrr = username.getText().toString();
        passss = password.getText().toString();

        //fill spinner
        Spinner sp = (Spinner)findViewById(R.id.spinactivity);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.activities, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapter);
        sp.setOnItemSelectedListener(this);
    }

    public void btnConfirmerinscription(View view){

        dbWorker dbw = new dbWorker(this);
        dbw.execute(userrr, passss, nomactivite);
        Toast.makeText(this, "Vous êtes actuellement inscrit, Merci!", Toast.LENGTH_SHORT).show();
    }

    public void btnCancelInscriptionActiv(View view){
        Intent i = new Intent(this, Dashboard.class);
        startActivity(i);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        String lactivite = parent.getItemAtPosition(position).toString();
        nomactivite=lactivite;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
