package com.example.ageeit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Dashboard extends AppCompatActivity {

    EditText txtUser;
    EditText txtPass;

    public String OldUser;
    public String OldPass;


    public void loadPrefs(EditText userEdt, EditText passEdt){
        int mode = Activity.MODE_PRIVATE;
        SharedPreferences myPrefs = getSharedPreferences("userInfos", mode);
        String user_value = myPrefs.getString("userS", "");
        String pass_value = myPrefs.getString("pwS", "");

        userEdt.setText(user_value);
        passEdt.setText(pass_value);

        OldUser=user_value;
        OldPass=pass_value;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        txtUser = findViewById(R.id.usernameEdt);
        txtPass = findViewById(R.id.passEdt);
        loadPrefs(txtUser, txtPass);
    }

    //async

    class dbWorker extends AsyncTask {

        private Context c;
        private AlertDialog ad;

        public dbWorker(Context c) {
            this.c = c;
        }

        @Override
        protected void onPreExecute() {
            this.ad = new AlertDialog.Builder(this.c).create();
            this.ad.setTitle("Status de mise à jour");
        }


        @Override
        protected Object doInBackground(Object[] param) {
            String cible = "http:192.168.0.118/phpscriptlab1/update.php";
            try {
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                String msg = URLEncoder.encode("olduser", "utf-8") + "=" +
                        URLEncoder.encode((String) param[0], "utf-8") + "&" +
                        URLEncoder.encode("newuser", "utf-8") + "=" +
                        URLEncoder.encode((String) param[1], "utf-8")+ "&" +
                        URLEncoder.encode("newpass", "utf-8") + "=" +
                        URLEncoder.encode((String) param[2], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while ((line = bufr.readLine()) != null) {
                    sbuff.append(line + "\n");
                }

                Log.d("Modif", sbuff.toString());

                return sbuff.toString();

            } catch (Exception ex) {
                return ex.getMessage();
            }


        }


        @Override
        protected void onPostExecute(Object o) {

            this.ad.setMessage("Modification réussie");
            this.ad.show();

        }

    }

    public void btnModif(View view) {
        dbWorker dbw = new dbWorker(this);
        dbw.execute(OldUser, this.txtUser.getText().toString(), this.txtPass.getText().toString());

    }

    public void btnActivities(View view){
        Intent i = new Intent(this, Activities.class);
        startActivity(i);
    }

    public void btnEnrollActivity(View view){
        Intent i = new Intent(this, Enrollement.class);
        startActivity(i);
    }

    public void btnStats(View view){
        Intent i = new Intent(this, Statistiques.class);
        startActivity(i);
    }

    public void btnListeetudiants(View view){
        Intent i = new Intent(this, Listeetudiants.class);
        startActivity(i);
    }

    public void btnNoteractiv(View view){
        Intent i = new Intent(this, notesactivity.class);
        startActivity(i);
    }
}
